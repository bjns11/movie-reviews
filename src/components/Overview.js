import React, { Component } from 'react';

import { NYTService } from '../services/NYT';

import Spinner from './Spinner';
import List from './List';
import Pagination from './Navigation/Pagination';
import Loader from './Loader';

class Overview extends Component {
	constructor(props) {
		super(props);

		this.service = new NYTService();

		this.state = {
			data: null,
			offset: 0,
			isLoading: false
		};
	}

	componentDidMount = async () => {
		const { searchTerm } = this.props;
		if (searchTerm.length) await this.fetchMovies();
	};

	componentDidUpdate = async (prevProps) => {
		if (prevProps.searchTerm !== this.props.searchTerm || prevProps.searchOrder !== this.props.searchOrder)
			await this.fetchMovies(this.props.searchTerm, this.props.searchOrder);
	};

	handlePagination = (offset) => {
		const { searchTerm, searchOrder } = this.props;

		this.setState({ offset });
		this.fetchMovies(searchTerm, searchOrder, offset);
	};

	fetchMovies = async (term, order, offset = 0) => {
		if (!term.length) this.setState({ data: null });
		else {
			this.setState({ isLoading: true }, async () => {
				const res = await this.service.searchMovie(term, order, offset);
				this.setState({ data: res.data, isLoading: false });
			});
		}
	};

	render() {
		const { data, offset, isLoading } = this.state;

		if (isLoading) return <Loader />;

		if (data === null) return null;

		if (!data.results.length)
			return (
				<div className="text-center" style={{ color: '#bbb' }}>
					No results
				</div>
			);

		return (
			<div className="Overview">
				<List movies={data.results} />
				<Pagination updateParentState={this.handlePagination} offset={offset} has_more={data.has_more} />
			</div>
		);
	}
}

export default Overview;
