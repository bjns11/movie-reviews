import React from 'react';

import noImage from '../../images/no-image_large.png';

import './Details.scss';

const Details = ({ movie }) => {
	const { Production, Director, Actors, Runtime, Genre, Poster, Plot, Ratings, imdbID } = movie;

	console.log(movie);

	const imgLink = Poster !== 'N/A' ? Poster : noImage;

	return (
		<div className="Details">
			<img src={imgLink} className="rounded mx-auto d-block" alt="poster"></img>
			<div className="section">
				<h4>Plot</h4>
				<p>{Plot}</p>
			</div>
			<div className="section">
				<h4>Genre</h4>
				<p>{Genre}</p>
			</div>
			<div className="section">
				<h4>Duration</h4>
				<p>{Runtime}</p>
			</div>
			<div className="section">
				<h4>Production</h4>
				<p>{Production}</p>
			</div>
			<div className="section">
				<h4>Director</h4>
				<p>{Director}</p>
			</div>
			<div className="section">
				<h4>Actors</h4>
				<p>{Actors}</p>
			</div>
			{Ratings && Ratings.length ? (
				<div className="section">
					<h4>Ratings</h4>
					<ul>
						{Ratings.map((rating, i) => (
							<li key={i}>
								{rating.Source}: {rating.Value}
							</li>
						))}
					</ul>
				</div>
			) : null}
			<div className="section">
				<h4>IMDb</h4>
				<a href={`https://www.imdb.com/title/${imdbID}/`} target="_blank" rel="noopener noreferrer">
					Link
				</a>
			</div>
		</div>
	);
};

export default Details;
