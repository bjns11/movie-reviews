import React from 'react';

import ContentLoader from 'react-content-loader';

const Loader = () => (
	<div className="Loader">
		<ContentLoader viewBox="0 0 130 60" backgroundColor={'#333'} foregroundColor={'#999'}>
			{/* Number of results */}
			<rect x="0" y="0" rx="1" ry="1" width="18" height="2" />

			{/* ### Loader 1 ### */}

			{/* Image */}
			<rect x="0" y="5" rx="1" ry="1" width="40" height="21" />

			{/* Title */}
			<rect x="2" y="29" rx="1" ry="1" width="30" height="2" />
			<rect x="2" y="32" rx="1" ry="1" width="22" height="2" />

			{/* Text */}
			<rect x="2" y="37" rx="0" ry="0" width="36" height="1" />
			<rect x="2" y="39" rx="0" ry="0" width="30" height="1" />
			<rect x="2" y="41" rx="0" ry="0" width="20" height="1" />
			<rect x="2" y="43" rx="0" ry="0" width="26" height="1" />

			{/* Buttons */}
			<rect x="1" y="48" rx="1" ry="1" width="18" height="4" />
			<rect x="21" y="48" rx="1" ry="1" width="18" height="4" />

			{/* ### Loader 2 ### */}

			{/* Image */}
			<rect x="45" y="5" rx="1" ry="1" width="40" height="21" />

			{/* Title */}
			<rect x="47" y="29" rx="1" ry="1" width="30" height="2" />
			<rect x="47" y="32" rx="1" ry="1" width="22" height="2" />

			{/* Text */}
			<rect x="47" y="37" rx="0" ry="0" width="36" height="1" />
			<rect x="47" y="39" rx="0" ry="0" width="30" height="1" />
			<rect x="47" y="41" rx="0" ry="0" width="20" height="1" />
			<rect x="47" y="43" rx="0" ry="0" width="26" height="1" />

			{/* Buttons */}
			<rect x="46" y="48" rx="1" ry="1" width="18" height="4" />
			<rect x="66" y="48" rx="1" ry="1" width="18" height="4" />

			{/* ### Loader 3 ### */}

			{/* Image */}
			<rect x="90" y="5" rx="1" ry="1" width="40" height="21" />

			{/* Title */}
			<rect x="92" y="29" rx="1" ry="1" width="30" height="2" />
			<rect x="92" y="32" rx="1" ry="1" width="22" height="2" />

			{/* Text */}
			<rect x="92" y="37" rx="0" ry="0" width="36" height="1" />
			<rect x="92" y="39" rx="0" ry="0" width="30" height="1" />
			<rect x="92" y="41" rx="0" ry="0" width="20" height="1" />
			<rect x="92" y="43" rx="0" ry="0" width="26" height="1" />

			{/* Buttons */}
			<rect x="91" y="48" rx="1" ry="1" width="18" height="4" />
			<rect x="111" y="48" rx="1" ry="1" width="18" height="4" />
		</ContentLoader>
	</div>
);

export default Loader;
