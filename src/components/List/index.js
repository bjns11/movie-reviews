import React, { useState } from 'react';

import './List.scss';

import Movie from '../Movie';
import Modal from '../Modal';
import Details from '../Details';

const List = ({ movies }) => {
	const [isOpen, setIsOpen] = useState(false);
	const [modalContent, setModlaContent] = useState({
		title: null,
		content: null
	});

	const openModal = (movie) => {
		setIsOpen(true);
		setModlaContent({
			title: movie.Title,
			content: <Details movie={movie} />
		});
	};

	const closeModal = () => {
		setIsOpen(false);
		setModlaContent({
			title: null,
			content: null
		});
	};

	const rows = [];
	const moviesPerRow = 3;

	for (let i = 0; i < Math.ceil(movies.length / moviesPerRow); i++) {
		const tmp = [];
		for (let k = i * moviesPerRow; k < Math.min((i + 1) * moviesPerRow, movies.length); k++) {
			tmp.push(<Movie key={k} movie={movies[k]} openModal={openModal} />);
		}
		rows.push(tmp);
	}

	return (
		<div className="List">
			<p className="count">Number of results: {movies.length}</p>
			{rows.map((row, i) => (
				<div className="row" key={i}>
					{row.map((movie, j) => (
						<div className="col-12 col-md-4 d-flex align-items-stretch" key={j}>
							{movie}
						</div>
					))}
				</div>
			))}
			<Modal isOpen={isOpen} handleClose={closeModal} title={modalContent.title}>
				{modalContent.content}
			</Modal>
		</div>
	);
};

export default List;
