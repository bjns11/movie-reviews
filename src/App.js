import React, { useState, useEffect } from 'react';
import ReactModal from 'react-modal';

import './App.scss';

import Navbar from './components/Navigation/Navbar';

import Overview from './components/Overview';
import Search from './components/Search';
import Footer from './components/Navigation/Footer';

function App() {
	const [searchTerm, setSearchTerm] = useState('');
	const [searchOrder, setSearchOrder] = useState('');

	useEffect(() => {
		ReactModal.setAppElement('body');
	});

	const handleInputChange = (term) => {
		setSearchTerm(term);
	};

	const handleOrderChange = (order) => {
		setSearchOrder(order);
	};

	return (
		<div className="App">
			<Navbar />
			<div className="container content">
				<Search updateParentTerm={handleInputChange} updateParentOrder={handleOrderChange} />
				<Overview searchTerm={searchTerm} searchOrder={searchOrder} />
			</div>
			<Footer />
		</div>
	);
}

export default App;
