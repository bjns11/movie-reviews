import { create } from 'axios';

export class OMDbService {
	constructor() {
		this.axios = create({
			baseURL: 'https://www.omdbapi.com'
		});
	}

	searchMovie = (title, year) => {
		return this.axios.get('/', {
			params: {
				apikey: 'e4f7540b',
				t: title,
				y: year,
				plot: 'full'
			}
		});
	};
}
