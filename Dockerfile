FROM node:alpine

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Copy package.json
COPY package.json /app

# Install node_modules
RUN npm install --silent

# Copy files
COPY . /app

CMD ["npm", "start"]